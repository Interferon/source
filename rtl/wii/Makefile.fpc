#
#   Makefile.fpc for Free Pascal WII RTL
#

[package]
main=rtl

[target]
loaders=
units=$(SYSTEMUNIT) uuchar objpas macpas $(ISO7185UNIT) $(EXTPASUNIT) $(STRINGSUNIT) \
      $(SORTBASEUNIT)  $(SYSUTILSUNIT) $(DOSUNIT) \
      $(CLASSESUNIT)  $(MATHUNIT) $(TYPINFOUNIT) $(CTYPESUNIT)  \
      $(CHARSETUNIT) $(CPALLUNIT) $(GETOPTSUNIT) \
      $(FPWIDESTRINGUNIT) $(CHARACTERUNIT) \
      $(TYPESUNIT) $(SYSCONSTUNIT) $(RTLCONSTSUNIT) $(LINEINFOUNIT) $(FGLUNIT) $(CMEMUNIT)
#rsts=math rtlconsts varutils typinfo variants classes sysconst dateutil fpmkunit
implicitunits=$(CP1250UNIT) $(CP1251UNIT) $(CP1252UNIT) $(CP1253UNIT) $(CP1254UNIT) $(CP1255UNIT) $(CP1256UNIT) $(CP1257UNIT) $(CP1258UNIT) \
      $(CP437UNIT) $(CP646UNIT) $(CP737UNIT) $(CP775UNIT) $(CP850UNIT) $(CP852UNIT) $(CP855UNIT) $(CP856UNIT) $(CP857UNIT) $(CP860UNIT) $(CP861UNIT) $(CP862UNIT) \
      $(CP863UNIT) $(CP864UNIT) $(CP865UNIT) $(CP866UNIT) $(CP869UNIT) $(CP874UNIT) $(CP3021UNIT) $(CP8859_1UNIT) $(CP8859_2UNIT) $(CP8859_3UNIT) $(CP8859_4UNIT) \
      $(CP8859_5UNIT) $(CP8859_6UNIT) $(CP8859_7UNIT) $(CP8859_8UNIT) $(CP8859_9UNIT) $(CP8859_10UNIT) $(CP8859_11UNIT) \
      $(CP8859_13UNIT) $(CP8859_14UNIT) $(CP8859_15UNIT) $(CP8859_16UNIT) $(CPKOI8_RUNIT) $(CPKOI8_UUNIT) \
      $(UNICODEDATAUNIT)

[require]
nortl=y

[install]
fpcpackage=y

[default]
target=wii
cpu=powerpc

[compiler]
includedir=$(INC) $(PROCINC)
sourcedir=$(INC) $(PROCINC) $(COMMON)


[prerules]
RTL=..
INC=$(RTL)/inc
COMMON=$(RTL)/common
PROCINC=$(RTL)/$(CPU_TARGET)
UNITPREFIX=rtl
SYSTEMUNIT=system
override FPC_SYSTEM_OPT+=@rtl.cfg

ifdef RELEASE
override FPCOPT+=-Ur
endif

# Paths
OBJPASDIR=$(RTL)/objpas

[rules]
.NOTPARALLEL:
# Get the system independent include file names.
# This will set the following variables :
# SYSINCNAMES
include $(INC)/makefile.inc
SYSINCDEPS=$(addprefix $(INC)/,$(SYSINCNAMES))

# Get the processor dependent include file names.
# This will set the following variables :
# CPUINCNAMES
include $(PROCINC)/makefile.cpu
SYSCPUDEPS=$(addprefix $(PROCINC)/,$(CPUINCNAMES))

# Put system unit dependencies together.
SYSDEPS=$(SYSINCDEPS) $(SYSCPUDEPS) $(INC)/softfpu.pp


#
# Base Units (System, strings, os-dependent-base-unit)
#

#
# Other system-dependent RTL Units
#

exec$(PPUEXT)    : exec.pp execf.inc execd.inc
	$(COMPILER) exec.pp

timer$(PPUEXT)   : timer.pp timerd.inc timerf.inc
	$(COMPILER) timer.pp

utility$(PPUEXT) : utility.pp exec$(PPUEXT) utilf.inc utild1.inc utild2.inc
	$(COMPILER) utility.pp

doslib$(PPUEXT)  : doslib.pp exec$(PPUEXT) timer$(PPUEXT) doslibd.inc doslibf.inc
	$(COMPILER) doslib.pp

hardware$(PPUEXT): hardware.pas exec$(PPUEXT)
	$(COMPILER) hardware.pas

inputevent$(PPUEXT): inputevent.pas exec$(PPUEXT) timer$(PPUEXT) utility$(PPUEXT)
	$(COMPILER) inputevent.pas

graphics$(PPUEXT): graphics.pas exec$(PPUEXT) utility$(PPUEXT) hardware$(PPUEXT)
	$(COMPILER) graphics.pas

layers$(PPUEXT)  : layers.pas exec$(PPUEXT) graphics$(PPUEXT) utility$(PPUEXT)
	$(COMPILER) layers.pas

intuition$(PPUEXT): intuition.pas exec$(PPUEXT) graphics$(PPUEXT) utility$(PPUEXT) \
                    inputevent$(PPUEXT) timer$(PPUEXT) layers$(PPUEXT)
	$(COMPILER) intuition.pas

aboxlib$(PPUEXT): aboxlib.pas
	$(COMPILER) aboxlib.pas

clipboard$(PPUEXT): clipboard.pas exec$(PPUEXT)
	$(COMPILER) clipboard.pas

datatype$(PPUEXT): datatypes.pas exec$(PPUEXT) doslib$(PPUEXT) intuition$(PPUEXT) \
                   utility$(PPUEXT) graphics$(PPUEXT)
	$(COMPILER) datatypes.pas

asl$(PPUEXT): asl.pas exec$(PPUEXT) graphics$(PPUEXT) utility$(PPUEXT)
	$(COMPILER) asl.pas

ahi$(PPUEXT): ahi.pas exec$(PPUEXT) utility$(PPUEXT)
	$(COMPILER) ahi.pas

mui$(PPUEXT): mui.pas exec$(PPUEXT) utility$(PPUEXT) intuition$(PPUEXT) graphics$(PPUEXT)
	$(COMPILER) mui.pas

tinygl$(PPUEXT): tinygl.pp exec$(PPUEXT)
	$(COMPILER) tinygl.pp

get9$(PPUEXT): get9.pas exec$(PPUEXT)
	$(COMPILER) get9.pas

muihelper$(PPUEXT): muihelper.pas intuition$(PPUEXT) mui$(PPUEXT) doslib$(PPUEXT) utility$(PPUEXT)
	$(COMPILER) muihelper.pas

